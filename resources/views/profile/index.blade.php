@extends('layout.master')

@section('judul')
Halaman Update Profile
@endsection

@section('content')

<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
      
    <div class="form-group">
      <label >Umur Profile</label>
      <input type="number" name="umur" value="{{$profile->umur}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



@endsection